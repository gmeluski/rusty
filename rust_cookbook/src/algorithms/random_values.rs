extern crate rand;

use rand::Rng;

pub fn main() {
    let mut range = rand::thread_rng();

    let n1: u8 = range.gen();
    let n2: u16 = range.gen();

    println!("Random u8: {}", n1);
    println!("Random u16: {}", n2);
    println!("Random u32: {}", range.gen::<u32>());
    println!("Random i32: {}", range.gen::<i32>());
    println!("Random float: {}", range.gen::<f64>());
}
